<%#                                                                          -%>
<%# File informations:                                                       -%>
<%# - Name:    elems/startup-setup.json.tt                                   -%>
<%# - Summary: Configuration for `startup-setup` chip.                       -%>
<%# - Authors:                                                               -%>
<%#   - Alessandro Molari <molari.alessandro@gmail.com> (alem0lars)          -%>
<%#   - Luca Molari <molari.luca@gmail.com> (LMolr)                          -%>
<%#                                                                          -%>
<%# Project informations:                                                    -%>
<%#   - Homepage:                                                            -%>
<%#       https://gitlab.com/fizzycfg/official-cfg/configs-chips             -%>
<%#   - Getting started: see README.md in the project root folder            -%>
<%#                                                                          -%>
<%# License: Apache v2.0                                                     -%>
<%#                                                                          -%>
<%# Licensed to the Apache Software Foundation (ASF) under one more          -%>
<%# contributor license agreements.  See the NOTICE file distributed with    -%>
<%# this work for additional information regarding copyright ownership.      -%>
<%# The ASF licenses this file to you under the Apache License, Version 2.0  -%>
<%# (the "License"); you may not use this file except in compliance with the -%>
<%# License.                                                                 -%>
<%# You may obtain a copy of the License at                                  -%>
<%#     http://www.apache.org/licenses/LICENSE-2.0                           -%>
<%# Unless required by applicable law or agreed to in writing, software      -%>
<%# distributed under the License is distributed on an "AS IS" BASIS,        -%>
<%# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied  -%>
<%# See the License for the specific language governing permissions and      -%>
<%# limitations under the License.                                           -%>
<%#                                                                          -%>
<%
  define_locals do
    prefixed("chips.startup_setup.") do
      variable :lastpass_username
      variable :web_apps
    end
  end
-%>

apps:
  # System
  <% if has_feature? :pulseaudio -%>
  - name: start-pulseaudio-x11
    single: pulseaudio
    detached: false
  <% end -%>
  <% if has_feature? :megasync -%>
  - name: megasync
  <% end -%>
  <% if has_feature? :compton -%>
  - name: compton
  <% end -%>
  <% if has_feature? :redshift -%>
  - name: "(/usr/lib/geoclue-2.0/demos/agent | redshift)"
    single: redshift
  <% end -%>
  <% if has_feature? :unclutter -%>
  - name: unclutter
    args: -root
  <% end -%>
  <% if has_feature? :nitrogen -%>
  - name: nitrogen
    args: --restore
    detached: false
  <% end -%>
  <% if has_feature? :udiskie -%>
  - name: udiskie
    args:
      - --no-automount
      - --no-notify
      - --tray
      - --use-udisks2
  <% end -%>

  # Misc
  <% if has_feature? :urxvtd -%>
  - name: urxvtd
  <% end -%>
  <% if has_feature? :dunst -%>
  - name: dunst
  <% end -%>
  <% if has_feature? :copyq -%>
  - name: copyq
  <% end -%>
  <% if has_feature? :networkmanager -%>
  - name: nm-applet
  <% end -%>
  <% if has_feature? :blueman -%>
  - name: blueman-applet
  <% end -%>
  <% if has_feature? :pulseaudio -%>
  - name: pasystray
  <% end -%>
  <% if has_feature? :nemo -%>
  - name: nemo
  <% end -%>

  # Docs
  <% if has_feature? :calibre -%>
  - name: calibre
  <% end -%>

  # Social
  <% if has_feature? :keybase -%>
  - name: keybase-gui
  <% end -%>
  <% if has_feature? :'telegram-multi' -%>
  - name: telegram-multi
  <% end -%>
  <% if has_feature? :hexchat -%>
  - name: hexchat
  <% end -%>
  <% if has_feature? :skype -%>
  - name: skypeforlinux
  <% end -%>
  <% if has_feature? :slack -%>
  - name: slack
  <% end -%>
  <% if has_feature? :discord -%>
  - name: discord
  <% end -%>
  <% if has_feature? :ripcord -%>
  - name: ripcord
  <% end -%>
  <% if has_feature? :thunderbird -%>
  - name: thunderbird
  <% end -%>

  # Development
  <% if has_feature?(:zeal) -%>
  - name: zeal
  <% end -%>

  # Web Apps
  <% if has_feature?("spawn-web-apps") -%>
  - name: spawn-web-apps
    args:
      - --only
      - <%= local!(:web_apps).join(",") %>
  <% end -%>

<% if has_feature? :lastpass -%>
lastpass:
  user: <%= local! :lastpass_username %>
<% end -%>
